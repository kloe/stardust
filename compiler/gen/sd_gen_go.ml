module A = Sd_anf
module I = Sd_io
module N = Sd_name
module S = Sd_syntax.Make (A)
module T = Sd_target

exception Missing_implementation of string

module Go = struct
  type definition =
    | Function of string * string list * statement list
    | Method of string * type_ * string * string list * statement list

  and statement =
    | Var of string * type_
    | Assign of string * expression
    | Expression of expression
    | Block of statement list
    | If of expression * statement list * statement list
    | Return of expression
    | Literal of string

  and expression =
    | Variable of string
    | Literal_bool of bool
    | Literal_string of string
    | Literal_int of int32
    | Field of expression * string
    | Cast of expression * type_
    | Call of expression * expression list
    | Lambda of string list * statement list

  and type_ =
    | Named_type of string
    | Function_type of type_ list * type_
    | Interface_type of (string * type_ list * type_) list

  let parens = Sd_gen_common.parens

  let rec format_definition w = function
    | Function (name, params, body) ->
        begin
          I.write_all w "func ";
          I.write_all w name;
          I.write_all w "(";
          List.iteri (fun i param -> begin
                        begin if i > 0 then I.write_all w ", " end;
                        I.write_all w param;
                        I.write_all w " interface{}";
                      end)
                     params;
          I.write_all w ") interface{} {\n";
          List.iter (format_statement w) body;
          I.write_all w "}\n";
        end

    | Method (receiver_name, receiver_type, name, params, body) ->
        begin
          I.write_all w "func (";
          I.write_all w receiver_name;
          I.write_all w " ";
          format_type w receiver_type;
          I.write_all w ") ";
          I.write_all w name;
          I.write_all w "(";
          List.iteri (fun i param -> begin
                        begin if i > 0 then I.write_all w ", " end;
                        I.write_all w param;
                        I.write_all w " interface{}";
                      end)
                     params;
          I.write_all w ") interface{} {\n";
          List.iter (format_statement w) body;
          I.write_all w "}\n";
        end

  and format_statement w = function
    | Var (name, type_) ->
        begin
          I.write_all w "var ";
          I.write_all w name;
          I.write_all w " ";
          format_type w type_;
          I.write_all w "\n";
        end

    | Assign (name, value) ->
        begin
          I.write_all w name;
          I.write_all w " = ";
          format_expression w 0 value;
          I.write_all w "\n";
        end

    | Expression expr ->
        begin
          format_expression w 0 expr;
          I.write_all w "\n";
        end

    | Block stmts ->
        begin
          I.write_all w "{\n";
          List.iter (format_statement w) stmts;
          I.write_all w "}\n";
        end

    | If (cond, if_true, if_false) ->
        begin
          I.write_all w "if ";
          format_expression w 0 cond;
          I.write_all w " {\n";
          List.iter (format_statement w) if_true;
          I.write_all w "} else {\n";
          List.iter (format_statement w) if_false;
          I.write_all w "}\n";
        end

    | Return expr ->
        begin
          I.write_all w "return ";
          format_expression w 0 expr;
          I.write_all w "\n";
        end

    | Literal text ->
        I.write_all w text

  and format_expression w p = function
    | Variable name ->
        I.write_all w name

    | Literal_bool bool ->
        I.write_all w (if bool then "true" else "false")

    | Literal_string string ->
        begin
          I.write_all w "\"";
          I.write_all w string;
          I.write_all w "\"";
        end

    | Literal_int int ->
        begin
          I.write_all w "int32(";
          I.write_all w (Int32.to_string int);
          I.write_all w ")";
        end

    | Field (expr, field) ->
        parens w 6 p @@ fun () -> begin
          format_expression w 6 expr;
          I.write_all w ".";
          I.write_all w field;
        end

    | Cast (expr, type_) ->
        parens w 6 p @@ fun () -> begin
          format_expression w 6 expr;
          I.write_all w ".(";
          format_type w type_;
          I.write_all w ")";
        end

    | Call (callee, args) ->
        parens w 6 p @@ fun () -> begin
          format_expression w 6 callee;
          I.write_all w "(";
          List.iter (fun arg -> begin
                       format_expression w 0 arg;
                       I.write_all w ", ";
                     end)
                    args;
          I.write_all w ")";
        end

    | Lambda (params, body) ->
        begin
          I.write_all w "func(";
          List.iter (fun param -> begin
                       I.write_all w param;
                       I.write_all w " interface{}, "
                     end)
                    params;
          I.write_all w ") interface{} {\n";
          List.iter (format_statement w) body;
          I.write_all w "}";
        end

  and format_type w = function
    | Named_type name ->
        I.write_all w name

    | Function_type (params, return) ->
        begin
          I.write_all w "func(";
          List.iter (fun param -> begin
                       format_type w param;
                       I.write_all w ", ";
                     end)
                    params;
          I.write_all w ") ";
          format_type w return;
        end

    | Interface_type methods ->
        begin
          I.write_all w "interface{";
          List.iter (fun (name, params, return) -> begin
                       I.write_all w name;
                       I.write_all w "(";
                       List.iter (fun param -> begin
                                    format_type w param;
                                    I.write_all w ", ";
                                  end)
                                 params;
                       I.write_all w ") ";
                       format_type w return;
                       I.write_all w ";";
                     end)
                    methods;
          I.write_all w "}";
        end
end

module Mangling : sig
  (**
   * Mangle a global, separating the namespace segments and name with slashes
   * and appending the arity if given.
   *)
  val global : int option -> string list -> string -> string

  (**
   * Like {!val:global}, but takes a {!type:N.qglobal}.
   *)
  val qglobal : int option -> N.qglobal -> string

  (**
   * Mangle a variable, so that it is a correct Go identifier.
   *)
  val variable : string -> string
end = struct
  let safe name =
    if String.length name >= 3 && String.sub name 0 3 = "SD_"
      then name else

    let is_safe c = (c >= 'a' && c <= 'z') ||
                    (c >= 'A' && c <= 'Z') ||
                    c = '_' in
    let rec go acc = function
      | "" -> acc
      | cs ->
          let c = String.get cs 0 in
          let c' = if is_safe c then String.make 1 c
                                else "φ" ^ string_of_int (Char.code c) in
          go (acc ^ c') (String.sub cs 1 (String.length cs - 1)) in
    go "" name

  let global p ns n =
    (^) "Γ" @@ safe @@ String.concat "/" (ns @ [n]) ^
      match p with Some p' -> "~" ^ string_of_int p' | None -> ""

  let qglobal p (`Qualified (ns, n)) =
    global p ns n

  let variable =
    safe
end

module Constants = struct
  (**
   * Name of the variable containing the Boolean value of a precondition or
   * postcondition.
   *)
  let sd_cond = "SD_COND"

  (**
   * Name of the variable containing the return value of a subroutine. This
   * variable is read by postconditions.
   *)
  let sd_out = "SD_OUT"
end

(**
 * The functions in this module correspond to those in the runtime.
 *)
module Rt : sig
  val assert_ : Go.expression -> Go.expression -> Go.statement
  val identity : Go.expression -> Go.expression

  val bool : Go.expression -> Go.expression
  val text : Go.expression -> Go.expression
  val rat_new : Go.expression -> Go.expression -> Go.expression
end = struct
  let assert_ name cond =
    Go.Expression (Go.Call (Go.Variable "SD_RT_ASSERT", [name; cond]))

  let identity expr =
    Go.Call (Go.Variable "SD_RT_IDENTITY", [expr])

  let bool value =
    Go.Call (Go.Variable "SD_RT_BOOL", [value])

  let text value =
    Go.Call (Go.Variable "SD_RT_TEXT", [value])

  let rat_new numer denom =
    Go.Call (Go.Variable "SD_RT_RAT_NEW", [numer; denom])
end

(** Generate an immediately-invoked function expression. *)
let iife names values body =
  Go.Call (Go.Lambda (names, body), values)

(** Generate a cast expression, but do not fail if the castee isn't of an
 * interface type.
 *)
let cast expr type_ =
  Go.Cast (Rt.identity expr, type_)

let rec gen_translation_unit tu =
  List.concat (List.map (gen_definition []) tu)

and gen_definition ns = function
  | S.Using_definition (_, _, defs) ->
      List.concat @@ List.map (gen_definition ns) defs

  | S.Namespace (name, defs) ->
      let ns' = ns @ [name] in
      List.concat @@ List.map (gen_definition ns') defs

  | S.Defsub (name, params, contract, body) ->
      let name'   = Mangling.global (Some (List.length params)) ns name in
      let params' = List.map Mangling.variable params in
      let body'   = gen_expression Constants.sd_out body in
      [Go.Function (name', params', gen_contract contract body')]

  | S.Defforeign (name, params, contract, impls) ->
      let name' = Mangling.global (Some (List.length params)) ns name in
      let params' = List.map Mangling.variable params in
      let impl = try List.assoc T.Go impls with
                 | Not_found -> raise (Missing_implementation name) in
      [Go.Function (name', params', gen_contract contract [Go.Literal impl])]

  | S.Defmeth (name, params) ->
      let name' = Mangling.global (Some (List.length params)) ns name in
      let params' = List.map Mangling.variable params in
      let params'' = List.map (fun p -> Go.Variable p) params' in
      let receiver =
        Go.Cast (List.hd params'', Go.Interface_type [
          ( name'
          , List.map (fun _ -> Go.Interface_type []) (List.tl params'')
          , Go.Interface_type [] )
        ]) in
      let params''' = List.tl params'' in
      let call = Go.Call (Go.Field (receiver, name'), params''') in
      [Go.Function (name', params', [Go.Return call])]

  | S.Defimpl (name, cls, params, body) ->
      let name'   = Mangling.qglobal (Some (List.length params)) name in
      let cls'    = match cls with
                    | `Qualified (["std"], "atom") -> Go.Named_type "SD_RT_ATOM"
                    | `Qualified (["std"], "bool") -> Go.Named_type "SD_RT_BOOL"
                    | `Qualified (["std"], "list") -> Go.Named_type "*SD_RT_LIST"
                    | `Qualified (["std"], "text") -> Go.Named_type "SD_RT_TEXT"
                    | `Qualified (["std"], "rat" ) -> Go.Named_type "SD_RT_RAT"
                    | _ -> raise @@ Failure "Invalid class" in
      let params' = List.map Mangling.variable params in
      let body'   = gen_expression Constants.sd_out body in

      let out = Go.Var (Constants.sd_out, Go.Interface_type []) in
      let impl = [out] @ body' @ [Go.Return (Go.Variable Constants.sd_out)] in
      [Go.Method (List.hd params', cls', name', List.tl params', impl)]

  | S.Docarticle (_, _) ->
      []

and gen_contract contract body =
  let assert_ (name, cond) =
        let cond_var = Go.Var (Constants.sd_cond, Go.Interface_type []) in
        let cond'    = gen_expression Constants.sd_cond cond in
        let assert_  = Rt.assert_ (Go.Literal_string name)
                                  (Go.Variable Constants.sd_cond) in
        Go.Block ([cond_var] @ cond' @ [assert_]) in
  let requires' = List.map assert_ @@ contract.S.requires in
  let ensures'  = List.map assert_ @@ contract.S.ensures  in

  let out = Go.Var (Constants.sd_out, Go.Interface_type []) in
  let return = Go.Return (Go.Variable Constants.sd_out) in
  requires' @ [out] @ body @ ensures' @ [return]

and gen_expression out = function
  | A.Value value ->
      let value' = gen_value value in
      [Go.Assign (Mangling.variable out, value')]

  | A.Let (name, value, body) ->
      let name'  = Go.Var (Mangling.variable name, Go.Interface_type []) in
      let value' = gen_expression name value in
      let body'  = gen_expression out body in
      [name'] @ value' @ body'

  | A.Apply (callee, args) ->
      let callee' = Mangling.qglobal (Some (List.length args)) callee in
      let args'   = List.map gen_value args in

      let call = Go.Call (Go.Variable callee', args') in
      [Go.Assign (Mangling.variable out, call)]

  | A.Call (callee, args) ->
      let callee' = gen_value callee in
      let args'   = List.map gen_value args in

      let callee_type = Go.Function_type ( List.map (fun _ -> Go.Interface_type []) args'
                                         , Go.Interface_type [] ) in
      let call = Go.Call (cast callee' callee_type, args') in
      [Go.Assign (Mangling.variable out, call)]

  | A.If (cond, if_true, if_false) ->
      let cond'     = gen_value cond in
      let if_true'  = gen_expression out if_true in
      let if_false' = gen_expression out if_false in

      let cond'' = cast cond' (Go.Named_type "SD_RT_BOOL") in
      [Go.If (cond'', if_true', if_false')]

and gen_value = function
  | A.Variable name ->
      Go.Variable (Mangling.variable name)

  | A.Literal (Sd_literal.Bool bool) ->
      Rt.bool (Go.Literal_bool bool)

  | A.Literal (Sd_literal.Text text) ->
      Rt.text (Go.Literal_string text)

  | A.Literal (Sd_literal.Rat (numer, denom)) ->
      Rt.rat_new (Go.Literal_int numer) (Go.Literal_int denom)

  | A.Closure (params, body) ->
      let params' = List.map Mangling.variable params in
      let body'   = gen_expression Constants.sd_out body in

      let out    = Go.Var (Constants.sd_out, Go.Interface_type []) in
      let return = Go.Return (Go.Variable Constants.sd_out) in
      Go.Lambda (params', [out] @ body' @ [return])

  | A.Out ->
      Go.Variable Constants.sd_out
