module I = Sd_io

module Callee_set = Sd_collect_callees.Callee_set

module A = Sd_anf
module N = Sd_name
module Literal = Sd_literal
module Target = Sd_target
module S = Sd_syntax.Make (A)

exception Missing_implementation of string

module Ecmascript = struct
  type statement =
    | Var of string * expression
    | Expression of expression
    | Return of expression
    | Literal of string
    | If of expression * statement list * statement list

  and expression =
    | Variable of string
    | Literal_boolean of bool
    | Literal_string of string
    | Literal_number of int32
    | Object
    | Field of expression * string
    | Call of expression * expression list
    | New of expression * expression list
    | Lambda of string list * statement list
    | Binary of expression * string * expression

  let parens = Sd_gen_common.parens

  (** Given a binary operator, return its precedence. *)
  let binary_precedence = function
    | "||" -> 5
    | "="  -> 3
    | _    -> 0

  let rec format_statement w = function
    | Var (name, init) ->
        begin
          I.write_all w "var ";
          I.write_all w name;
          I.write_all w " = ";
          format_expression w 0 init;
          I.write_all w ";\n";
        end

    | Expression expr ->
        begin
          format_expression w 0 expr;
          I.write_all w ";\n";
        end

    | Return value ->
        begin
          I.write_all w "return ";
          format_expression w 0 value;
          I.write_all w ";\n";
        end

    | Literal text ->
        I.write_all w text

    | If (cond, if_true, if_false) ->
        begin
          I.write_all w "if (";
          format_expression w 0 cond;
          I.write_all w ") {\n";
          List.iter (format_statement w) if_true;
          I.write_all w "} else {\n";
          List.iter (format_statement w) if_false;
          I.write_all w "}\n";
        end

  and format_expression w p = function
    | Variable name ->
        I.write_all w name

    | Literal_boolean bool ->
        I.write_all w (if bool then "true" else "false")

    | Literal_string string ->
        begin
          I.write_all w "\"";
          I.write_all w string;
          I.write_all w "\"";
        end

    | Literal_number number ->
        I.write_all w (Int32.to_string number)

    | Object ->
        I.write_all w "({})"

    | Field (obj, field) ->
        parens w 19 p @@ fun () -> begin
          format_expression w 19 obj;
          I.write_all w "[\"";
          I.write_all w field;
          I.write_all w "\"]";
        end

    | Call (callee, args) ->
        parens w 19 p @@ fun () -> begin
          format_expression w 19 callee;
          I.write_all w "(";
          List.iteri (fun i arg -> begin
                        begin if i > 0 then I.write_all w ", " end;
                        format_expression w 2 arg;
                      end)
                     args;
          I.write_all w ")";
        end

    | New (constructor, args) ->
        parens w 19 p @@ fun () -> begin
          I.write_all w "new ";
          format_expression w 19 constructor;
          I.write_all w "(";
          List.iteri (fun i arg -> begin
                        begin if i > 0 then I.write_all w ", " end;
                        format_expression w 2 arg;
                      end)
                     args;
          I.write_all w ")";
        end

    | Lambda (params, body) ->
        begin
          I.write_all w "(function(";
          List.iteri (fun i param -> begin
                        begin if i > 0 then I.write_all w ", " end;
                        I.write_all w param;
                      end)
                     params;
          I.write_all w ") {\n";
          List.iter (format_statement w) body;
          I.write_all w "})";
        end

    | Binary (left, op, right) ->
        let s = binary_precedence op in
        parens w s p @@ fun () -> begin
          format_expression w s left;
          I.write_all w " ";
          I.write_all w op;
          I.write_all w " ";
          format_expression w s right;
        end
end

module E = Ecmascript

let (%%) a b = E.Field (a, b)

(** Generate an immediately-invoked function expression. *)
let iife names values body =
  E.Call (E.Lambda (names, body), values)

module Mangling : sig
  (**
   * Mangle a global, separating the namespace segments and name with slashes
   * and appending the arity if given.
   *)
  val global : int option -> string list -> string -> string

  (**
   * Like {!val:global}, but takes a {!type:N.qglobal}.
   *)
  val qglobal : int option -> N.qglobal -> string

  (**
   * Mangle a variable, so that it is a correct ECMAScript identifier.
   *)
  val variable : string -> string
end = struct
  let global p ns n =
    String.concat "/" (ns @ [n]) ^
      match p with Some p' -> "~" ^ string_of_int p' | None -> ""

  let qglobal p (`Qualified (ns, n)) =
    global p ns n

  let variable name =
    if String.length name >= 3 && String.sub name 0 3 = "SD_"
      then name else

    let is_safe c = (c >= 'a' && c <= 'z') ||
                    (c >= 'A' && c <= 'Z') ||
                    c = '_' in
    let rec go acc = function
      | "" -> acc
      | cs ->
          let c = String.get cs 0 in
          let c' = if is_safe c then String.make 1 c
                                else "$" ^ string_of_int (Char.code c) in
          go (acc ^ c') (String.sub cs 1 (String.length cs - 1)) in
    go "" name
end

module Constants = struct
  (**
   * The global Stardust object, [window.stardust]. This object contains
   * user-defined subroutines.
   *)
  let sd = "SD"

  (**
   * The global object, may be used by foreign subroutines.
   *)
  let sd_global = "SD_GLOBAL"

  (**
   * The runtime object, containing runtime facilities such as core data
   * structures and method bookkeeping.
   *)
  let sd_rt = "SD_RT"

  (**
   * Name of the variable containing the Boolean value of a precondition or
   * postcondition.
   *)
  let sd_cond = "SD_COND"

  (**
   * Name of the variable containing the return value of a subroutine. This
   * variable is read by postconditions.
   *)
  let sd_out = "SD_OUT"
end

(**
 * The functions in this module correspond to those in the runtime.
 *)
module Rt : sig
  val assert_ : E.expression -> E.expression -> E.statement
  val define_method : string -> E.expression
  val implement_method : string -> string -> E.expression -> E.statement
  val register_callee : int * N.qglobal -> E.statement
  val rat : E.expression
end = struct
  let assert_ name cond =
    let call = E.Call (E.Variable Constants.sd_rt %% "assert", [name; cond]) in
    E.Expression call

  let define_method name =
    E.Call ( E.Variable Constants.sd_rt %% "defineMethod"
           , [E.Literal_string name] )

  let implement_method name cls impl =
    let call = E.Call ( E.Variable Constants.sd_rt %% "implementMethod"
                      , [E.Literal_string name; E.Literal_string cls; impl] ) in
    E.Expression call

  let register_callee (nparams, qglobal) =
    let mangled = Mangling.qglobal (Some nparams) qglobal in
    let call = E.Call ( E.Variable Constants.sd_rt %% "registerCallee"
                      , [E.Literal_string mangled] ) in
    E.Expression call

  let rat = E.Variable Constants.sd_rt %% "Rat"
end

let rec gen_translation_unit defs callees =
  let defs'    = List.concat @@ List.map (gen_definition []) defs in
  let globals  = [ E.Variable "window"
                 ; E.Variable "window" %% "stardust"
                 ; E.Variable "window" %% "stardust" %% "rt" ] in
  let callregs = List.map Rt.register_callee (Callee_set.elements callees) in
  [ E.Expression (E.Literal_string "use strict")
  ; E.Expression (iife [Constants.sd_global; Constants.sd; Constants.sd_rt]
                       globals (defs' @ callregs)) ]

and gen_definition ns = function
  | S.Using_definition (_, _, defs) ->
      List.concat @@ List.map (gen_definition ns) defs

  | S.Namespace (name, defs) ->
      let ns' = ns @ [name] in
      List.concat @@ List.map (gen_definition ns') defs

  | S.Defsub (name, params, contract, body) ->
      let name'   = Mangling.global (Some (List.length params)) ns name in
      let params' = List.map Mangling.variable params in
      let body'   = gen_expression Constants.sd_out body in
      let init    = E.Lambda (params', gen_contract contract body') in
      [E.Expression (E.Binary (E.Variable Constants.sd %% name', "=", init))]

  | S.Defforeign (name, params, contract, impls) ->
      let name' = Mangling.global (Some (List.length params)) ns name in
      let params' = List.map Mangling.variable params in
      let impl = try List.assoc Target.Ecmascript impls with
                 | Not_found -> raise (Missing_implementation name') in
      let init = E.Lambda (params', gen_contract contract [E.Literal impl]) in
      [E.Expression (E.Binary (E.Variable Constants.sd %% name', "=", init))]

  | S.Defmeth (name, params) ->
      let name' = Mangling.global (Some (List.length params)) ns name in
      let init = Rt.define_method name' in
      [E.Expression (E.Binary (E.Variable Constants.sd %% name', "=", init))]

  | S.Defimpl (name, cls, params, body) ->
      let name'   = Mangling.qglobal (Some (List.length params)) name in
      let cls'    = Mangling.qglobal None cls in
      let params' = List.map Mangling.variable params in
      let body'   = gen_expression Constants.sd_out body in

      let return  = E.Return (E.Variable Constants.sd_out) in
      let impl    = E.Lambda (params', body' @ [return]) in
      [Rt.implement_method name' cls' impl]

  | S.Docarticle (_, _) ->
      []

and gen_contract contract body =
  let assert_ (name, cond) =
        let cond'   = gen_expression Constants.sd_cond cond in
        let assert_ = Rt.assert_ (E.Literal_string name)
                                 (E.Variable Constants.sd_cond) in
        cond' @ [assert_] in
  let requires' = List.concat @@ List.map assert_ @@ contract.S.requires in
  let ensures'  = List.concat @@ List.map assert_ @@ contract.S.ensures  in
  requires' @ body @ ensures' @ [E.Return (E.Variable Constants.sd_out)]

and gen_expression out = function
  | A.Value value ->
      let value' = gen_value value in
      [E.Var (Mangling.variable out, value')]

  | A.Let (name, value, body) ->
      let value' = gen_expression (Mangling.variable name) value in
      let body'  = gen_expression out body in
      value' @ body'

  | A.Apply (callee, args) ->
      let callee' = Mangling.qglobal (Some (List.length args)) callee in
      let args'   = List.map gen_value args in

      let call = E.Call (E.Variable Constants.sd %% callee', args') in
      [E.Var (Mangling.variable out, call)]

  | A.Call (callee, args) ->
      let callee' = gen_value callee in
      let args'   = List.map gen_value args in

      let call = E.Call (callee', args') in
      [E.Var (Mangling.variable out, call)]

  | A.If (cond, if_true, if_false) ->
      let cond'     = gen_value cond in
      let if_true'  = gen_expression out if_true in
      let if_false' = gen_expression out if_false in
      (* TODO: Assert condition is Boolean. *)
      [E.If (cond', if_true', if_false')]

and gen_value = function
  | A.Variable name ->
      E.Variable (Mangling.variable name)

  | A.Literal (Literal.Bool bool) ->
      E.Literal_boolean bool

  | A.Literal (Literal.Text text) ->
      E.Literal_string text

  | A.Literal (Literal.Rat (numer, denom)) ->
      E.New (Rt.rat, [E.Literal_number numer; E.Literal_number denom])

  | A.Closure (params, body) ->
      let params' = List.map Mangling.variable params in
      let body'   = gen_expression Constants.sd_out body in
      (* TODO: Assert argument count. *)
      let return = E.Return (E.Variable Constants.sd_out) in
      E.Lambda (params', body' @ [return])

  | A.Out ->
      E.Variable Constants.sd_out
