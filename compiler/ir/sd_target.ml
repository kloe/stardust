(** Supported targets. *)

type t =
  | Ecmascript
  | Go
