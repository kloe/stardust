type t =
  | Bool of bool
  | Text of string
  | Rat  of int32 * int32
