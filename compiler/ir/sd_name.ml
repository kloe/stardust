(** Names of things. *)

(** Name of a global, always qualified. *)
type qglobal = [
  | `Qualified of string list * string
]

(** Name of a global, potentially qualified. *)
type uglobal = [
  | `Qualified   of string list * string
  | `Unqualified of string
]
