(**
 * Data structures for abstract syntax trees.
 *
 * Types are parameterized over type of global, which may be
 * {!type:Sd_name.qglobal} or {!type:Sd_name.uglobal}.
 *)

module T = Sd_target

(**
 * A calculus, such as lambda calculus or administrative normal form.
 *)
module type Calculus = sig
  type 'g expression
end

(**
 * Used for documentation.
 *)
type xml =
  | Xml_text of string
  | Xml_element of string * xml list

module Make (Calc : Calculus) = struct
  (** A translation unit is a list of definitions. *)
  type 'g translation_unit = 'g definition list

  (** Definitions occur at the top level. They define things. *)
  and 'g definition =
    | Using_definition of string list * string list * 'g definition list
    | Namespace of string * 'g definition list
    | Defsub of string * string list * 'g contract * 'g Calc.expression
    | Defforeign of string * string list * 'g contract * (T.t * string) list
    | Defmeth of string * string list
    | Defimpl of 'g * 'g * string list * 'g Calc.expression
    | Docarticle of string * xml list

  (** Contracts are checked before and after a subroutine executes. *)
  and 'g contract = { requires: (string * 'g Calc.expression) list
                    ; ensures:  (string * 'g Calc.expression) list }
end
