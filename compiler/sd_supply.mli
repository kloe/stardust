type t

val start : unit -> t
val next : t -> int
