(**
 * The sequentialization analysis phase turns expressions in lambda calculus
 * into expressions in administrative normal form. See {!module:Sd_lambda} and
 * {!module:Sd_anf} for more information on these different calculi.
 *
 * The functions in this module take a supply, which is used for generating
 * names for temporaries, to be bound in let bindings.
 *)

val sequentialize_translation_unit
  :  Sd_supply.t
  -> 'g Sd_syntax.Make (Sd_lambda).translation_unit
  -> 'g Sd_syntax.Make (Sd_anf).translation_unit

val sequentialize_definition
  :  Sd_supply.t
  -> 'g Sd_syntax.Make (Sd_lambda).definition
  -> 'g Sd_syntax.Make (Sd_anf).definition

val sequentialize_contract
  :  Sd_supply.t
  -> 'g Sd_syntax.Make (Sd_lambda).contract
  -> 'g Sd_syntax.Make (Sd_anf).contract

val sequentialize_expression
  :  Sd_supply.t
  -> 'g Sd_lambda.expression
  -> 'g Sd_anf.expression
