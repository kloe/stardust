(** Callee collection is used for callee registration. *)

module Callee_set : Set.S
  with type elt = int * Sd_name.qglobal

val collect_translation_unit
  :  Sd_name.qglobal Sd_syntax.Make (Sd_lambda).translation_unit
  -> Callee_set.t

val collect_definition
  :  Sd_name.qglobal Sd_syntax.Make (Sd_lambda).definition
  -> Callee_set.t

val collect_contract
  :  Sd_name.qglobal Sd_syntax.Make (Sd_lambda).contract
  -> Callee_set.t

val collect_expression
  :  Sd_name.qglobal Sd_lambda.expression
  -> Callee_set.t
