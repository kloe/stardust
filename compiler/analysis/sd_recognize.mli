(**
 * Recognition is the process by which S-expressions are converted into
 * abstract syntax trees.
 *)

exception Unrecognized_definition
exception Unrecognized_expression

val recognize_translation_unit
  :  Sd_sexp.t list
  -> Sd_name.uglobal Sd_syntax.Make (Sd_lambda).translation_unit

val recognize_definition
  :  Sd_sexp.t
  -> Sd_name.uglobal Sd_syntax.Make (Sd_lambda).definition

val recognize_contract
  :  (Sd_sexp.t * Sd_sexp.t) list
  -> Sd_name.uglobal Sd_syntax.Make (Sd_lambda).contract

val recognize_expression
  :  Sd_sexp.t
  -> Sd_name.uglobal Sd_lambda.expression
