module C = Sd_collect_callees
module G_ecmascript = Sd_gen_ecmascript
module G_go = Sd_gen_go
module G_html = Sd_gen_html.Make (Sd_lambda)
module I = Sd_io
module L = Sd_lex
module P = Sd_parse
module Q = Sd_qualify
module R = Sd_recognize
module S = Sd_sequentialize

module Supply = Sd_supply

exception Invalid_usage

let () =
  let supply = Sd_supply.start () in

  let lexbuf  = Lexing.from_channel stdin in
  let sexps   = P.sexps_eof L.token lexbuf in
  let tu      = R.recognize_translation_unit sexps in
  let qtu     = Q.qualify_translation_unit tu in
  let anf     = S.sequentialize_translation_unit supply qtu in
  match Sys.argv with
  | [| _; "--target=ecmascript" |] ->
      let callees = C.collect_translation_unit qtu in
      let es = G_ecmascript.gen_translation_unit anf callees in
      let w  = I.writer_of_out_channel stdout in
      List.iter (G_ecmascript.Ecmascript.format_statement w) es

  | [| _; "--target=go" |] ->
      let es = G_go.gen_translation_unit anf in
      let w  = I.writer_of_out_channel stdout in
      List.iter (G_go.Go.format_definition w) es

  | [| _; "--target=html" |] ->
      let w = I.writer_of_out_channel stdout in
      G_html.gen_translation_unit w qtu

  | _ ->
      raise Invalid_usage
