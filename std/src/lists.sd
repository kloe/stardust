; NOTE: The following subroutines must be defined using defforeign: cons, nil.
;       Applications of these subroutines are generated when using quoting.

## using std (= bool? int? list list? seq)
## using std/closures (identity)
## using std/algebra/groups (<+> <zero> monoid?)
## using std/algebra/rings (+)

## namespace std

(defforeign list? (x)
  {ensure {"bool?" (bool? (out))}}
  {ecmascript "var SD_OUT = x instanceof SD_RT.List;\n"
   go "_, ok := x.(*SD_RT_LIST)\nSD_OUT = SD_RT_BOOL(ok)\n"})

## namespace lists

(docarticle "std/lists/cons~2"
  (p "Given a value " (code "hd") " and a list " (code "tl") ", "
     (code "(cons hd tl)") " evaluates to the list " (code "tl") " but with "
     (code "hd") " added to the front."))
(defforeign cons (hd tl)
  {require {"list?" (list? tl)}
   ensure  {"list?" (list? (out))}}
  {ecmascript "var SD_OUT = new SD_RT.Cons(hd, tl);\n"
   go "SD_OUT = &SD_RT_LIST{Nil: false, Head: hd, Tail: tl.(*SD_RT_LIST)}\n"})

(docarticle "std/lists/nil~0"
  (p (code "(nil)") " evaluates to the empty list. It may also be written "
     (code "'()") ", which is short for " (code "(quote ())") "."))
(defforeign nil ()
  {ensure {"list?" (list? (out))}}
  {ecmascript "var SD_OUT = new SD_RT.Nil();\n"
   go "SD_OUT = &SD_RT_LIST{Nil: true}\n"})

(defforeign empty? (xs)
  {require {"list?" (list? xs)}
   ensure  {"bool?" (bool? (out))}}
  {ecmascript "var SD_OUT = xs instanceof SD_RT.Nil;\n"
   go "SD_OUT = SD_RT_BOOL(xs.(*SD_RT_LIST).Nil)\n"})

(defsub singleton (x)
  {ensure {"list?" (list? (out))}}
  (cons x '()))

(defforeign foldl (xs z f)
  {require {"list?" (list? xs)}}
  {ecmascript "var SD_OUT = SD_RT.List.foldl(xs, z, f);\n"
   go "SD_OUT = SD_RT_LIST_FOLDL(xs, z, f)\n"})

(defsub fold (m xs)
  {require {"monoid? m" (monoid? m)
            "list? xs"  (list? xs)}}
  (foldl xs (<zero> m) (closure (a b) (<+> m a b))))

(defsub length (xs)
  {require {"list?" (list? xs)}
   ensure  {"int?" (int? (out))}}
  (foldl xs 0 (closure (acc x) (+ acc 1))))

(defsub all (xs)
  {require {"list?" (list? xs)}
   ensure  {"bool?" (bool? (out))}}
  (all xs (identity)))

(defsub all (xs f)
  {require {"list?" (list? xs)}
   ensure  {"bool?" (bool? (out))}}
  (foldl xs #t (closure (acc x) (and* acc (call f x)))))

(defsub any (xs)
  {require {"list?" (list? xs)}
   ensure  {"bool?" (bool? (out))}}
  (any xs (identity)))

(defsub any (xs f)
  {require {"list?" (list? xs)}
   ensure  {"bool?" (bool? (out))}}
  (foldl xs #f (closure (acc x) (or* acc (call f x)))))

(defsub zip (xs ys)
  {require {"list? xs" (list? xs)
            "list? ys" (list? ys)}
   ensure  {"list?"    (list? (out))}}
  (zip-with xs ys (closure (x y) (cons x (cons y '())))))

(defforeign zip-with (xs ys f)
  {require {"list? xs" (list? xs)
            "list? ys" (list? ys)}
   ensure  {"list?"    (list? (out))}}
  {ecmascript "var SD_OUT = SD_RT.List.zipWith(xs, ys, f);\n"
   go "SD_OUT = SD_RT_LIST_ZIP_WITH(xs, ys, f)\n"})

(defsub each (xs f)
  {require {"list?" (list? xs)}}
  (foldl xs '() (closure (acc x) (seq (call f x) acc))))

(defimpl = list (xs ys)
  (-=- xs ys))

(defforeign -=- (xs ys)
  {ecmascript "var SD_OUT = SD_RT.List[\"=\"](xs, ys);\n"
   go "SD_OUT = SD_RT_BOOL(SD_RT_LIST_EQUIV(xs, ys))\n"})
