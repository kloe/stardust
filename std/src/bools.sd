## namespace std

(defforeign bool? (x)
  ; NOTE: Cannot have the usual bool? postcondition because that would lead to
  ;       infinite recursion.
  {ecmascript "var SD_OUT = typeof x === \"boolean\";\n"
   go "_, ok := x.(SD_RT_BOOL)\nSD_OUT = SD_RT_BOOL(ok)\n"})

(namespace bools
  (defsub case (a b)
    (closure (x) (if x a b)))

  (defsub bless (x)
    {require {"bool?" (std/bool? x)}}
    (if x (closure (a b) b)
          (closure (a b) a)))

  (defsub curse (x)
    (call x #f #t))
)

(defsub not (x)
  {require {"bool?" (bool? x)}
   ensure  {"bool?" (bool? (out))}}
  (if x #f #t))

(defsub but-not (x y)
  {require {"bool? x" (bool? x)
            "bool? y" (bool? y)}
   ensure  {"bool?" (bool? (out))}}
  (and x (not y)))

(defsub implies (x y)
  {require {"bool? x" (bool? x)
            "bool? y" (bool? y)}
   ensure  {"bool?" (bool? (out))}}
  (or (not x) y))

(defsub and (x y)
  {require {"bool? x" (bool? x)
            "bool? y" (bool? y)}
   ensure  {"bool?" (bool? (out))}}
  (if x y #f))

(defsub nand (x y)
  {require {"bool? x" (bool? x)
            "bool? y" (bool? y)}
   ensure  {"bool?" (bool? (out))}}
  (not ## and x y))

(defsub or (x y)
  {require {"bool? x" (bool? x)
            "bool? y" (bool? y)}
   ensure  {"bool?" (bool? (out))}}
  (if x #t y))

(defsub nor (x y)
  {require {"bool? x" (bool? x)
            "bool? y" (bool? y)}
   ensure  {"bool?" (bool? (out))}}
  (not ## or x y))

(defsub xor (x y)
  {require {"bool? x" (bool? x)
            "bool? y" (bool? y)}
   ensure  {"bool?" (bool? (out))}}
  (or (but-not x y)
      (but-not y x)))

(defsub nxor (x y)
  {require {"bool? x" (bool? x)
            "bool? y" (bool? y)}
   ensure  {"bool?" (bool? (out))}}
  (not ## xor x y))

(defsub complement (f)
  (closure (x) (not ## call f x)))

(defimpl = bool (x y)
  (nxor x y))

(defimpl <=> bool (x y)
  (if x (if y 'eq 'gt)
        (if y 'lt 'eq)))

(defimpl hash bool (x)
  (hash (if x 't 'f)))
