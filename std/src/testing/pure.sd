## namespace std/testing

(docarticle "std/testing/tree~2"
  (p "Tests are organized in a tree. " (code "(tree name children)") " sets "
     "up a tree of tests. A tree is not itself an executable test, but "
     "rather groups tests under a name. " (code "name") " must be a text "
     "value and " (code "children") " must be a list of mixed test trees "
     "and test leaves."))

(defsub tree (name children)
  {'type     'tree
   'name     name
   'children children})

(docarticle "std/testing/leaf~2"
  (p "Tests are organized in a tree." (code "(leaf name test)") " sets up "
     "a test leaf. A test leaf is an executable test and has no children. "
     (code "name") " must be a text value and " (code "test") " must be a "
     "unary closure whose argument can be forwarded to subroutines such as "
     (code "assert") "."))

(defsub leaf (name test)
  {'type 'leaf
   'name name
   'todo #f
   'test test})

(docarticle "std/testing/leaf-todo~2"
  (p "Like " (code "leaf") ", but fail if all assertions succeed, and "
     "succeed if at least one assertion fails."))

(defsub leaf-todo (name test)
  {'type 'leaf
   'name name
   'todo #t
   'test test})

(docarticle "std/testing/assert~2"
  (p (code "(assert t c)") " makes the test represented by the opaque value "
     (code "t") ", as provided by " (code "leaf") ", pass or fail depending "
     "on the Boolean value " (code "c") ". When " (code "c") " is true the "
     "test passes and otherwise it fails."))

(defsub assert (t c)
  (call (std/dicts/lookup! t 'assert) c))
